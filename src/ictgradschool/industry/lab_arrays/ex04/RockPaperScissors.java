package ictgradschool.industry.lab_arrays.ex04;

import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;

    public void start() {

        String name = getUserName("What is your name?"); //Gets the user's name by using the 'getter' method getUserName;
        int choice = getUserChoice("1. Rock \n2. Scissors \n3. Paper \n4. Quit \nEnter choice: "); //Gets the user's choice by using the 'getter' method getUserChoice;

        displayPlayerChoice(name, choice); //Calls the method displayPlayerChoice;

        int playerChoice = choice;
        int computerChoice = PAPER;

        userWins(playerChoice, computerChoice);

        getResultString(playerChoice, computerChoice);

    }

    private String getUserName(String prompt) {
        System.out.print(prompt);
        String userName = Keyboard.readInput();
        return userName;
    }

    private int getUserChoice (String prompt) {
        System.out.print(prompt);
        String userChoice = Keyboard.readInput();
        int userChoiceInt = Integer.parseInt(userChoice);
        return userChoiceInt;
    }


    public void displayPlayerChoice(String name, int choice) {

        if (choice == ROCK) {
            System.out.println(name + " chose rock.");
        }   else if (choice == SCISSORS) {
            System.out.println(name + " chose scissors.");
        }   else if (choice == PAPER) {
            System.out.println(name + "  chose paper.");
        }   else {
            System.out.println("Thank you for playing. Goodbye.");
        }

    }

    public boolean userWins(int playerChoice, int computerChoice) {

        if (playerChoice == 1 && computerChoice == 2) {
            return true;
        }   else if (playerChoice == 2 && computerChoice == 3) {
            return true;
        }   else if (playerChoice == 3 && computerChoice == 1) {
            return true;
        }   else {
            return false;
        }

    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.

        if (playerChoice == 1 && computerChoice == 2) {
            return ROCK_WINS;
        }

        return null;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
