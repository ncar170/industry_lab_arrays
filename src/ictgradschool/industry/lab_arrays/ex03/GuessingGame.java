package ictgradschool.industry.lab_arrays.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.

        double randomNumber = Math.random() * 100;
        int randomInt = (int)randomNumber;

        int goal = randomInt;

        int guess = 0;

        while (guess != goal ) {
            System.out.println("Please guess a number between 1 and 100:");
            String guess1 = Keyboard.readInput();
            int guess2 = Integer.parseInt(guess1);
            if (guess2 > goal) {
                System.out.println("Too high, try again.");
            }   else if (guess2 < goal) {
                System.out.println("Too low, try again.");
            }   else {
                System.out.println("Perfect!");
                break;
            }
        }

        System.out.println("Goodbye");

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
