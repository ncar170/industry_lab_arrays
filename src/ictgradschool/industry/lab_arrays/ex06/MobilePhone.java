package ictgradschool.industry.lab_arrays.ex06;

public class MobilePhone {

    private String brand;
    private String model;
    private double price;
    
    public MobilePhone(String b, String m, double p) {
        brand = b;
        model = m;
        price = p;
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    // TODO Insert getModel() method here

    public String getModel() {
        return model;
    }
    // TODO Insert setModel() method here

    public void setModel(String model) {
        this.model = model;
    }

    // TODO Insert getPrice() method here

    public double getPrice() {
        return price;
    }

    // TODO Insert setPrice() method here

    public void setPrice(double price) {
        this.price = price;
    }

    // TODO Insert toString() method here

    @Override
    public String toString() {
        return super.toString();
    }

    // TODO Insert isCheaperThan() method here

    public boolean isCheaperThan() {
        return true;
    }
    
    // TODO Insert equals() method here


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}


